import * as React from 'react';
import style from './CodeEditor.scss';
import AceEditor from 'react-ace';
import 'brace/mode/python';
import 'brace/theme/xcode';
import 'brace/ext/language_tools';
import * as AceAjax from 'brace';
import { CodeError } from './Home';
import _ from 'lodash';

interface Props {
  onChange(text: string): void;
  addBreakpoint(row: number): void;
  removeBreakpoint(row: number): void;
  value: string;
  debugLine: number;
  errors: CodeError[];
  debugging: boolean;
}

interface AceEditorElement extends HTMLElement {
  editor: AceAjax.Editor;
}

interface AceEditorEvent<T> {
  getDocumentPosition: () => AceAjax.Position;
  editor: AceAjax.Editor;
  defaultPrevented: boolean;
  propagationStopped: boolean;
  domEvent: T;
}

const EDITOR_CURRENT_LINE_CLASS = 'editor-current-line';

export class CodeEditor extends React.Component<Props> {
  private ref = React.createRef<HTMLDivElement>();
  private editor: AceAjax.Editor;

  shouldComponentUpdate(nextProps: Readonly<Props>): boolean {
    if (nextProps.debugging !== this.props.debugging || nextProps.debugLine !== this.props.debugLine) {
      return true;
    }
    if (!_.isEqual(nextProps.errors, this.props.errors)) {
      return true;
    }
    return false;
  }

  public componentDidUpdate(prevProps: Props): void {
    if (prevProps.debugLine !== this.props.debugLine) {
      if (prevProps.debugLine >= 0) {
        this.editor.session.removeGutterDecoration(prevProps.debugLine, EDITOR_CURRENT_LINE_CLASS);
      }
      if (this.props.debugLine >= 0) {
        this.editor.session.addGutterDecoration(this.props.debugLine, EDITOR_CURRENT_LINE_CLASS);
      }
    }
    if (!_.isEqual(prevProps.errors, this.props.errors)) {
      const errors = this.props.errors.map(({ row, text }) => ({ row, text, type: 'error', column: 0 }));
      this.editor.session.setAnnotations(errors);
    }
  }

  public componentDidMount(): void {
    // eslint-disable-next-line react/no-string-refs
    this.editor = (this.refs.aceEditor as AceEditorElement).editor;
    this.editor.session.setUseWorker(false);
    this.editor.on('guttermousedown', (e: AceEditorEvent<MouseEvent>) => {
      if (e.domEvent.clientX > 20) {
        return;
      }
      e.defaultPrevented = true;
      e.propagationStopped = true;
      e.domEvent.stopImmediatePropagation();
      e.domEvent.preventDefault();
      const row = e.getDocumentPosition().row;
      if (e.editor.session.getBreakpoints()[row]) {
        e.editor.session.clearBreakpoint(row);
        this.props.removeBreakpoint(row);
      } else {
        e.editor.session.setBreakpoint(row, 'editor-breakpoint');
        this.props.addBreakpoint(row);
      }
    });
  }

  private onChange = (newValue: string) => {
    this.props.onChange(newValue);
    // TODO: fix cleared annotations issue
    setTimeout(() => {
      const errors = this.props.errors.map(({ row, text }) => ({ row, text, type: 'error', column: 0 }));
      this.editor.session.setAnnotations(errors);
    });
  };

  /* eslint-disable react/no-string-refs */
  public render() {
    const { debugging } = this.props;
    return (
      <div className={style.root} ref={this.ref}>
        <AceEditor
          ref="aceEditor"
          readOnly={debugging}
          mode="python"
          theme="xcode"
          name="editor"
          width="100% !important"
          onChange={this.onChange}
          value={this.props.value}
          fontSize={14}
          enableBasicAutocompletion={true}
          enableLiveAutocompletion={true}
          editorProps={{
            $blockScrolling: Infinity,
          }}
          setOptions={{
            useWorker: false,
            spellcheck: false,
          }}
        />
      </div>
    );
  }
}
