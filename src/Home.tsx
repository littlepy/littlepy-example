import * as React from 'react';
import style from './Home.scss';
import { CodeEditor } from './CodeEditor';
import { DebugPanel } from './DebugPanel';
import { LittlePy, PyModule, PyMachine, PyScope } from 'littlepy';

export interface DebugVariable {
  name: string;
  value: string;
}

const CODE_EXAMPLE = `
max = 25

for val in range(1, max):
    isPrime = True
    for n in range(2, val): 
        if (val % n) == 0: 
            isPrime = False
            break
    if isPrime:
        print(val)
`;

export interface CodeError {
  row: number;
  text: string;
}

interface State {
  debugging: boolean;
  code: string;
  debugLine: number;
  variables: DebugVariable[];
  errors: CodeError[];
  logLinesCount: number;
}

export class Home extends React.Component<{}, State> {
  public state = {
    debugging: false,
    code: CODE_EXAMPLE,
    debugLine: -1,
    variables: [],
    errors: [],
    logLinesCount: 0,
  };

  private logLines: string[] = [];
  private module: PyModule;
  private machine: PyMachine;
  private breakpoints: number[] = [];

  public componentDidMount(): void {
    this.onChange(this.state.code);
  }

  private stop = () => {
    this.machine.stop();
  };

  private createMachine() {
    this.machine = LittlePy.createMachine(
      {
        main: this.module,
      },
      this.breakpoints.map(row => ({
        moduleId: this.module.id,
        row,
      })),
    );
    this.machine.onWriteLine = line => {
      console.log('OUTPUT: ', line);
      this.logLines = [...this.logLines, line];
      this.setState({
        logLinesCount: this.state.logLinesCount + 1,
      });
    };
  }

  private updateDebugState() {
    if (!this.machine) {
      if (this.state.debugging) {
        this.setState({
          debugging: false,
        });
      }
      return;
    }
    const row = this.machine.getPosition().row;
    const scope = this.machine.getCurrentScope();
    const variables: DebugVariable[] = [];
    function fillScope(scope: PyScope) {
      for (const key of Object.keys(scope.objects)) {
        variables.push({
          name: key,
          value: scope.objects[key].toString(),
        });
      }
    }
    fillScope(scope);
    this.setState({
      debugLine: row,
      variables,
    });
  }

  private run = () => {
    if (this.state.debugging) {
      return;
    }
    this.createMachine();
    this.logLines = [];
    this.setState(
      {
        debugging: true,
        debugLine: -1,
        logLinesCount: this.state.logLinesCount + 1,
      },
      () => {
        this.machine.startCallModule('main', undefined);
        this.machine.run();
        this.machine = undefined;
        this.setState({
          debugging: false,
        });
      },
    );
  };

  private debug = () => {
    if (this.state.debugging) {
      return;
    }
    this.createMachine();
    this.logLines = [];
    this.setState(
      {
        debugging: true,
        logLinesCount: this.state.logLinesCount + 1,
        debugLine: -1,
      },
      () => {
        this.machine.startCallModule('main', () => {
          this.machine = undefined;
          this.setState({
            debugging: false,
            variables: [],
            debugLine: -1,
          });
        });
        this.updateDebugState();
      },
    );
  };

  private resume = () => {
    if (this.machine) {
      this.machine.debug();
      this.updateDebugState();
    }
  };

  private stepOver = () => {
    if (this.machine) {
      this.machine.debugOver();
      this.updateDebugState();
    }
  };

  private stepInto = () => {
    if (this.machine) {
      this.machine.debugIn();
      this.updateDebugState();
    }
  };

  private stepOut = () => {
    if (this.machine) {
      this.machine.debugOut();
      this.updateDebugState();
    }
  };

  private addBreakpoint = (row: number) => {
    if (this.breakpoints.findIndex(t => t === row) >= 0) {
      return;
    }
    this.breakpoints.push(row);
  };

  private removeBreakpoint = (row: number) => {
    const index = this.breakpoints.findIndex(t => t === row);
    if (index >= 0) {
      this.breakpoints.splice(index, 1);
    }
  };

  private onChange = (text: string) => {
    const { module } = LittlePy.compileModule(text, 'main');
    this.module = module;
    this.setState({
      code: text,
      errors: module.errors.map(({ prefix, row }) => ({ text: prefix, row })),
    });
  };

  public render() {
    const { debugging, debugLine, errors, variables, code } = this.state;
    return (
      <div className={style.root}>
        <CodeEditor onChange={this.onChange} value={code} debugLine={debugLine} debugging={debugging} errors={errors} addBreakpoint={this.addBreakpoint} removeBreakpoint={this.removeBreakpoint} />
        <DebugPanel
          logLines={this.logLines}
          debugging={debugging}
          stop={this.stop}
          debug={this.debug}
          resume={this.resume}
          run={this.run}
          stepInto={this.stepInto}
          stepOut={this.stepOut}
          stepOver={this.stepOver}
          variables={variables}
          hasErrors={errors.length > 0}
        />
      </div>
    );
  }
}
