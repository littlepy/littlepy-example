import * as React from 'react';
import { render } from 'react-dom';
import { Root } from './Root';
import 'assets/style.scss';

render(
  <Root />,
  document.getElementById('app'),
);

// Hot Module Replacement

if (module.hot) {
  module.hot.accept('./Root', () => {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const NewRoot = require('./Root').Root;
    render(
      <NewRoot />,
      document.getElementById('app'),
    );
  });
}
