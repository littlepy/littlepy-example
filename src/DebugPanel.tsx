import * as React from 'react';
import style from './DebugPanel.scss';
import { DebugVariable } from './Home';
import _ from 'lodash';

interface Props {
  debugging: boolean;
  hasErrors: boolean;
  variables: DebugVariable[];
  logLines: string[];

  run(): void;
  debug(): void;
  stop(): void;
  resume(): void;
  stepOver(): void;
  stepInto(): void;
  stepOut(): void;
}

export class DebugPanel extends React.Component<Props> {
  shouldComponentUpdate(nextProps: Readonly<Props>): boolean {
    if (
      nextProps.debugging !== this.props.debugging ||
      nextProps.hasErrors !== this.props.hasErrors ||
      nextProps.variables.length !== this.props.variables.length ||
      this.props.logLines !== nextProps.logLines
    ) {
      return true;
    }
    if (!_.isEqual(this.props.variables, nextProps.variables)) {
      return true;
    }
    return false;
  }

  public render() {
    const { debugging, hasErrors, run, debug, stop, resume, stepOver, stepInto, stepOut, variables, logLines } = this.props;
    return (
      <div className={style.root}>
        <div className={style.actions}>
          {!debugging && (
            <button disabled={hasErrors} onClick={run}>
              Run
            </button>
          )}
          {!debugging && (
            <button disabled={hasErrors} onClick={debug}>
              Debug
            </button>
          )}
          {debugging && <button onClick={resume}>Resume</button>}
          {debugging && <button onClick={stepOver}>Step Over</button>}
          {debugging && <button onClick={stepInto}>Step Into</button>}
          {debugging && <button onClick={stepOut}>Step Out</button>}
          {debugging && <button onClick={stop}>Stop</button>}
        </div>
        <div className={style.variables}>
          <div className={style.header}>Variables</div>
          <div className={style.variablesBody}>
            <div className={style.names}>
              {variables.map(({ name }) => (
                <div className={style.varRow} key={name}>
                  {name}
                </div>
              ))}
            </div>
            <div className={style.values}>
              {variables.map(({ name, value }) => (
                <div className={style.varRow} key={name}>
                  {value}
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className={style.log}>
          <div className={style.header}>Output</div>
          <div className={style.logContainer}>
            <div className={style.logRoot}>
              <div className={style.logScroll}>
                {logLines.map((line, index) => (
                  <div key={index} className={style.logLine}>
                    {line}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
