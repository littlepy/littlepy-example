import * as React from 'react';
import { shallow } from 'enzyme';
import { Home } from '../../src/Home';

describe('Home', () => {
  it('should render page', () => {
    const homeComponent = shallow(<Home />);
    expect(homeComponent).toBeTruthy();
  });
});
